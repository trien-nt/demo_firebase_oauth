import React from 'react';
import './App.css';
import uuidv4 from 'uuidv4'
import logo from "./logo.svg";
import UserInfo from "./components/UserInfo";

class App extends React.Component {
  // constructor(props) {
  //   super(props);
  //   this.init();
  //   this.state = {
  //     ytVideoInstanceMaps: {},
  //     ytLinkListObj: [],
  //   };
  //   this.ytLinkList = [
  //     '01',
  //     '02',
  //     '03',
  //     '04',
  //     '05',
  //     '06',
  //     '07',
  //     '08',
  //   ];
  // }
  // getYtLinkListObj = () => {
  //   let listObj = [];
  //   let id = null;
  //   this.ytLinkList.forEach((videoLink, index) => {
  //       id = index;
  //       listObj.push({ videoLink, id });
  //   });
  //   return listObj;
  // }
  // componentDidMount() {
  //   const ytLinkListObj = this.getYtLinkListObj();
  //       this.setState({
  //           ytLinkListObj,
  //       });

  //   window['onYouTubeIframeAPIReady'] = (e) => {
  //     this.YT = window['YT'];
  //     var players = new Array(ytLinkListObj.length);
  //     for(var i = 0; i<ytLinkListObj.length;i++){
  //       players[i] = new window['YT'].Player(ytLinkListObj[i].videoLink, {
  //         events: {
  //           'onReady': this.onPlayerReady.bind(this,i),
  //           'onStateChange': this.onPlayerStateChange.bind(this,i)
  //         }
  //       });
  //     }
  //     // ytLinkListObj.map(item =>{
  //     //   players[item.id] = new window['YT'].Player(item.videoLink, {
  //     //     events: {
  //     //       'onReady': this.onPlayerReady.bind(this,item.id),
  //     //       'onStateChange': this.onPlayerStateChange.bind(this,item.id)
  //     //     }
  //     //   });
  //     // })
      
  //   };
  // }
  // displayYtVideoCard = () => {
  //   const { ytLinkListObj } = this.state;
  //   return ytLinkListObj.map(item => (
  //      <>
  //         <iframe id={item.videoLink}
  //           width="640" height="360"
  //           src="https://www.youtube.com/embed/M7lc1UVf-VE?enablejsapi=1"
  //           // frameborder="0"
  //           style={{ border: "solid 4px #37474F" }}
  //           title="abc"
  //         ></iframe>
  //      </>
  //   ));
  // }
  // render(){
  //   return (
  //     // <iframe id={this.props.x}
  //     //       width="640" height="360"
  //     //       src="https://www.youtube.com/embed/M7lc1UVf-VE?enablejsapi=1"
  //     //       // frameborder="0"
  //     //       style={{ border: "solid 4px #37474F" }}
  //     //       title="abc"
  //     //     ></iframe>
  //     <>
  //     {this.displayYtVideoCard()}
  //     </>
  //   );
  // }
 
  // init() {
  //   var tag = document.createElement('script');
  //   // tag.id = 'iframe-demo';
  //   tag.src = 'https://www.youtube.com/iframe_api';
  //   var firstScriptTag = document.getElementsByTagName('script')[0];
  //   firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  // }

  // onPlayerReady(i,event) {
  //   console.log("id ne: " + i )
  //   this.state.ytLinkListObj.map(item =>{
  //     document.getElementById(item.videoLink).style.borderColor = '#FF6D00';
  //   })
  // }
  // changeBorderColor(i,playerStatus) {
  //   console.log("change mau : "+ playerStatus)
  //   var color;
  //   this.state.ytLinkListObj.map(item =>{
  //     if(i === item.id){
  //       if (playerStatus === -1) {
  //         color = "#37474F"; // unstarted = gray
  //       } else if (playerStatus === 0) {
  //         color = "#FFFF00"; // ended = yellow
  //       } else if (playerStatus === 1) {
  //         color = "#33691E"; // playing = green
  //       } else if (playerStatus === 2) {
  //         color = "#DD2C00"; // paused = red
  //       } else if (playerStatus === 3) {
  //         color = "#AA00FF"; // buffering = purple
  //       } else if (playerStatus === 5) {
  //         color = "#FF6DOO"; // video cued = orange
  //       }
  //       if (color) {
  //         document.getElementById(item.videoLink).style.borderColor = color;
  //       }
  //     }
      
  //   })
  //   // if (playerStatus === -1) {
  //   //   color = "#37474F"; // unstarted = gray
  //   // } else if (playerStatus === 0) {
  //   //   color = "#FFFF00"; // ended = yellow
  //   // } else if (playerStatus === 1) {
  //   //   color = "#33691E"; // playing = green
  //   // } else if (playerStatus === 2) {
  //   //   color = "#DD2C00"; // paused = red
  //   // } else if (playerStatus === 3) {
  //   //   color = "#AA00FF"; // buffering = purple
  //   // } else if (playerStatus === 5) {
  //   //   color = "#FF6DOO"; // video cued = orange
  //   // }
  //   // if (color) {
  //   //   document.getElementById(this.props.x).style.borderColor = color;
  //   // }
  // }
  // onPlayerStateChange(i,event) {
  //   console.log("event: " +i)
  //   this.changeBorderColor(i,event.data);
  // }

  render() {
    return (
      <div className="App">
        <UserInfo />
      </div>
    );
  }

}

export default App;
